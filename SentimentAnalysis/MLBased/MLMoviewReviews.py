import nltk

# Split Corpus
# Negative and Positive reviews are loaded in
negativeReviewsFileName = "./rt-polaritydata/rt-polarity.neg"
positiveReviewsFileName = "./rt-polaritydata/rt-polarity.pos"

with open(negativeReviewsFileName, 'r') as f:
    negativeReviews = f.readlines()

with open(positiveReviewsFileName, 'r') as f:
    positiveReviews = f.readlines()

print("Reading the training/test data")

testTraingingSplitIndex = 2500

testNegativeReviews = negativeReviews[testTraingingSplitIndex + 1:]
testPositiveReviews = positiveReviews[testTraingingSplitIndex + 1:]

print("Splitting the training/test data")

trainingNegativeReviews = negativeReviews[:testTraingingSplitIndex]
trainingPositiveReviews = positiveReviews[:testTraingingSplitIndex]

# Define Vocabulary of Training Data
# splits the training data into words for the vocabulary
positiveWordList = [word for line in trainingPositiveReviews for word in line.split()]
negativeWordList = [word for line in trainingNegativeReviews for word in line.split()]
allWordList = [item for sublist in [positiveWordList, negativeWordList] for item in sublist]
vocabulary = list(set(allWordList))

# Extract the features
# takes in a review and returns a feature vector
def extract_features(review):
    review_words = set(review)
    features={}
    for word in vocabulary:
        features[word]=(word in review_words)
    return features

# Using the Classifier
# Creating a simple wrapper function to abstract the details
def naiveBayesSentimentCalculator(NBClassifier, review):
    problemInstance = review.split()
    problemFeature = extract_features(problemInstance)
    return NBClassifier.classify(problemFeature)


# Test Harness
def getTestReviewSentiments(NBClassifier):
    testNegResults = [naiveBayesSentimentCalculator(NBClassifier, review) for review in testNegativeReviews]
    testPosResults = [naiveBayesSentimentCalculator(NBClassifier, review) for review in testPositiveReviews]
    labelToNum = {'positive':1, 'negative':-1}
    numericNegResults = [labelToNum[x] for x in testNegResults]
    numericPosResults = [labelToNum[x] for x in testPosResults]
    return {'results-on-positive':numericPosResults, 'results-on-negative':numericNegResults}


def runDiagnostics(reviewResults):
    positiveReviewResults = reviewResults['results-on-positive']
    negativeReviewResults = reviewResults['results-on-negative']
    numTruePositive = sum(x > 0 for x in positiveReviewResults)
    numTrueNegative = sum(x < 0 for x in negativeReviewResults)
    pctTruePositive = float(numTruePositive)/len(positiveReviewResults)
    pctTrueNegative = float(numTrueNegative)/len(negativeReviewResults)
    totalAccurate = numTruePositive + numTrueNegative
    total = len(positiveReviewResults) + len(negativeReviewResults)
    print("Accuracy on positive reviews + " + "%.2f" % (pctTruePositive*100) + "%")
    print("Accuracy on negative reviews + " + "%.2f" % (pctTrueNegative*100) + "%")
    print("Overall accuracy = " + "%.2f" % (totalAccurate*100/total) + "%")


# Setting up the training data
# a list of tuples: first element in each tuple is the review, second is the label
def getTrainingData():
    negTaggedTrainingReviewList = [{'review': oneReview.split(), 'label': 'negative'}
                                   for oneReview in trainingNegativeReviews]
    posTaggedTrainingReviewList = [{'review': oneReview.split(), 'label': 'positive'}
                                   for oneReview in trainingPositiveReviews]
    fullTaggedTrainingData = [item for sublist in [negTaggedTrainingReviewList, posTaggedTrainingReviewList]
                              for item in sublist]
    trainingData = [(review['review'], review['label']) for review in fullTaggedTrainingData]
    return trainingData


def getTrainedNaiveBayesClassifier(extract_features, trainingData):
    # Training Features
    traingingFeatures = nltk.classify.apply_features(extract_features, trainingData)
    # Train the classifier
    trainedNBC = nltk.NaiveBayesClassifier.train(traingingFeatures)
    return trainedNBC

def main():
    # Get the training data from the reviews
    trainingData = getTrainingData()
    print("Created training data ")

    # Train a classifier
    trainedNBClassifier = getTrainedNaiveBayesClassifier(extract_features, trainingData)
    print("Created trained NB classifier")

    # get test results
    results = getTestReviewSentiments(trainedNBClassifier)
    print("testing results created")

    runDiagnostics(results)


if __name__ == "__main__":
    main()