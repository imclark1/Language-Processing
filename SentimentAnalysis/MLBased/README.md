# ML Based Approach to Solving Sentiment Analysis
This is an implimentation of Niave Bayes Theorem to classify more than 10,000 movie reviews from a Cornell movie review dataset.

## Download Corpus
We will be using pre-classified reviews from the Cornell movie review dataset
* http://www.cs.cornell.edu/people/pabo/movie-review-data/
We will use ```sentence-polarit-dataset-v1.0```

# Approach
* Split Corpus
    * Test and trainging datasets
* Define Vocabulary
    * Set of all words (Training Data Only)
* Extract Features
    * Create word tuples (Use Vocabulary)
* Train Classifier
    * Done using nltk (Training Data Only)
* Classify Test Data
    * Done using nltk (Test Data)
* Measure Accuray
    * On test data

# Results
* runs ~5 min
* Output: <br/>
    ` Accuracy on positive reviews + 73.39% ` <br/>
    ` Accuracy on negative reviews + 77.07% ` <br/>
    ` Overall accuracy = 75.23% `