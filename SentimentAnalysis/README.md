# Sentiment Analysis Using Rule and Machine Learning Based Analysis
In this project is an example of rule based and machine learning based sentiment analysis using nltk and vader libraries <br/>
The Data set is a set of movie reviews and their sentiment values that is provided by the Cornell (http://www.cs.cornell.edu/people/pabo/movie-review-data/)