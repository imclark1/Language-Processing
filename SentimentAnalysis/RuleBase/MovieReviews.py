from nltk.sentiment import vader
from nltk.corpus import sentiwordnet as swn
from string import punctuation
from nltk.corpus import stopwords

# Load vader sentiment analyzer
sia = vader.SentimentIntensityAnalyzer()

stopwords = set(stopwords.words('english') + list(punctuation))

# Load the positive and negative reviews into lists, from the downloaded dataset
positiveReviewsFileName = "./rt-polaritydata/rt-polarity.pos"

with open(positiveReviewsFileName, 'r') as f:
    positiveReviews = f.readlines()

negativeReviewsFileName = "./rt-polaritydata/rt-polarity.neg"

with open(negativeReviewsFileName, 'r') as f:
    negativeReviews = f.readlines()


def vaderSentiment(review):
    return sia.polarity_scores(review)['compound']


def NaiveSentiment(review):
    reviewPolarity = 0.0
    numExceptions = 0
    for word in review.lower().split():
        numMeanings = 0
        if word in stopwords:
            continue
        weight = 0.0
        try:
            for meaning in list(swn.senti_synsets(word)):
                if meaning.pos_score() > meaning.neg_score():
                    weight = weight + (meaning.pos_score() - meaning.neg_score())
                    numMeanings = numMeanings + 1
                elif meaning.pos_score() < meaning.neg_score():
                    weight = weight - (meaning.neg_score() - meaning.pos_score())
                    numMeanings = numMeanings + 1
        except:
            numExceptions = numExceptions + 1
        if numMeanings > 0:
            reviewPolarity = reviewPolarity + (weight/numMeanings)
    return reviewPolarity


def getReviewSentiments(vaderSentimentCalculator):
    negAnalysis = [vaderSentimentCalculator(oneNegativeReview) for oneNegativeReview in negativeReviews]
    posAnalysis = [vaderSentimentCalculator(onePositiveReview) for onePositiveReview in positiveReviews]
    return {'results-on-positive' :posAnalysis, 'results-on-negative' :negAnalysis}


def runDiagnostics(sentimentResults):
    posResults = sentimentResults['results-on-positive']
    negResults = sentimentResults['results-on-negative']
    pctTruePositive = float(sum(x > 0 for x in posResults)) / len(posResults)
    pctTrueNegative = float(sum(x < 0 for x in negResults)) / len(negResults)
    totalAccurate = float(sum(x > 0 for x in posResults)) + float(sum( x < 0 for x in negResults))
    total = len(posResults) + len(negResults)
    print("Accuracy on positive reviews = " + "%.2f" % (pctTruePositive*100) + "%")
    print("Accuracy on negative reviews = " + "%.2f" % (pctTrueNegative*100) + "%")
    print("Overall accuracy = " + "%.2f" % (totalAccurate*100/total) + "%")


def main():
    print("This is a Sentiment Analysis of Cornell Movie Review Dataset: ")
    runDiagnostics(getReviewSentiments(NaiveSentiment))


if __name__ == "__main__":
    main()

