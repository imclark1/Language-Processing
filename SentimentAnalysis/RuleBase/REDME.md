# Rule base Sentiment Analysis
This is a rule-base sentiment analysis example on classifying movie reviews using VADER.

## Download Corpus
We will be using pre-classified reviews from the Cornell movie review dataset
* http://www.cs.cornell.edu/people/pabo/movie-review-data/
We will use `sentence-polarit-dataset-v1.0`

# Approach
* Load data into VADER
* Run a Naive Rule Based Sentiment on the data that returns the review polarity on all pos and neg reviews (Acounting for punctuation and stopwords)
* using these analyses, run a diagnostic and see how accurate the results where to the assigned rating of the review
* Print out the results 

# Results
* Takes ~5-10 sec
* Output: <br/>
`Accuracy on positive reviews = 75.56% ` <br/>
`Accuracy on negative reviews = 42.79% ` <br/>
`Overall accuracy = 59.17%`